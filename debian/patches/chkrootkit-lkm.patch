From: Richard Lewis <richard.lewis.debian@googlemail.com>
Date: Tue, 21 Feb 2023 19:49:21 +0000
Subject: chkrootkit: lkm

chkutmp and chkproc call 'ps', and per the comments in chkutmp this assumes
that this is safe. this patch adds the directory passed by -p to patH before calling those tools
so that a known good ps can be used if it is available.

To support -p commands in $cmdlist are meant to be called only as
$cmd, but there were several places where this was not done: so replace cut with $cut, etc

Move test for existence of chkdirs and chkproc later so that other subtests can still
run if neither is present

Make it clearer which command produced output

Allow chkdirs to be used with -r (and better message if it is
skipped).  There is no need to combine the eligibility checks for
chkproc and chkdirs -- test them independently. The setting if PV is
onky needed for chkproc

there is no need to skip chkdirs on SunOS (just set PV=0), and the check of the
FreeBSD versionn did not make sense


Cope with the case where none of the dirs to be checked exists (the ls
| tr pipeline is broken in that case, and ls | tr is pretty pointless
anyway: just use chkdir if the dir exists. This supports running
chkrootkit on termux, where none of the directories exist)

Forwarded: yes
(Forwarded by email: 21 Dec 2024)
---
 chkrootkit | 138 ++++++++++++++++++++++++++++++++++---------------------------
 1 file changed, 77 insertions(+), 61 deletions(-)

diff --git a/chkrootkit b/chkrootkit
index 7aaa93d..3c41625 100755
--- a/chkrootkit
+++ b/chkrootkit
@@ -348,72 +348,88 @@ PORT="114 145 465 511 600 1008 1524 1999 1978 2881 3049 3133 3879 4000 4369 5190
    fi
 }
 
-lkm ()
-{
+lkm (){
+    if [ "${EXPERT}" = "t" ]; then
+        [ -r "/proc/$KALLSYMS" ] &&  ${egrep} -i "adore|sebek" < "/proc/$KALLSYMS" 2>/dev/null
+        [ -d /proc/knark ] &&  "${ls}" -la /proc/knark 2> /dev/null
+        PV=$("${ps}" -V 2>/dev/null| "${cut}" -d " " -f 3 |"${awk}" -F . '{ print $1 "." $2 $3 }' | "${awk}" '{ if ($0 > 3.19) print 3; else if ($0 < 2.015) print 1; else print 2 }')
+        [ "$PV" = "" ] &&  PV=2
+        [ "${SYSTEM}" = "SunOS" ] && PV=0
+        expertmode_output "./chkproc -v -v -p $PV"
+        return 5
+    fi
+    [ "$QUIET" != "t" ] && echo "started"
+    _start "Adore LKM"
+    if [ -r "/proc/$KALLSYMS" ]; then
+        if ${egrep} -i adore < "/proc/$KALLSYMS" >/dev/null 2>&1; then
+            _warn "Adore LKM installed\n"
+        else
+            _not_found
+        fi
+    else
+        _not_tested
+    fi
+
+    _start "sebek LKM (Adore based)"
+    if [ -r "/proc/$KALLSYMS" ]; then
+        if ${egrep} -i sebek < "/proc/$KALLSYMS" >/dev/null 2>&1; then
+            _warn "Sebek LKM installed\n"
+        else
+            _not_found
+        fi
+    else
+        _not_tested
+    fi
+
+    lookfor_rootkit "knark LKM" "" "/proc/knark"
+
     prog=""
-    if [  \( "${SYSTEM}" = "Linux"  -o \( "${SYSTEM}" = "FreeBSD" -a \
-       `echo ${V} | ${awk} '{ if ($1 > 4.3 || $1 < 6.0) print 1; else print 0 }'` -eq 1 \) \) -a "${ROOTDIR}" = "/" ]; then
-       [  -x ./chkproc -a "`find /proc -maxdepth 1 2>/dev/null| wc -l`" -gt 1 ] && prog="./chkproc"
-      [  -x ./chkdirs ] && prog="$prog ./chkdirs"
-      if [ "$prog" = "" -o ${mode} = "pm" ]; then
-         echo "not tested: can't exec $prog"
-         return ${NOT_TESTED}
-      fi
+    _start "for hidden processes with chkproc"
+    if [ ! -x ./chkproc ]; then
+        _warn "chkproc not tested: can't exec ./chkproc"
+    elif [ "${mode}" != rt ]; then
+        _not_tested
+    else
+        if [ "$SYSTEM" = "Linux" ]; then
+            F=$($ps -V 2>/dev/null | wc -w)
+            PV=$($ps -V 2>/dev/null| $cut -d " " -f "$F" | "${awk}" -F . '{ print $1 "." $2 $3 }' | "${awk}" '{ if ($0 > 3.19) print 3; else if ($0 < 2.11) print 1; else print 2 }')
+        fi
+        [ "$PV" = "" ] &&  PV=2
+        [ "${SYSTEM}" = "SunOS" ] && PV=0
+        if [ "${DEBUG}" = "t" ]; then
+            echo "*** PV=$PV ***"
+        fi
 
-      if [ "${EXPERT}" = "t" ]; then
-         [ -r /proc/$KALLSYMS ] &&  ${egrep} -i "adore|sebek" < /proc/$KALLSYMS 2>/dev/null
-         [ -d /proc/knark ] &&  ${ls} -la /proc/knark 2> /dev/null
-         PV=`$ps -V 2>/dev/null| $cut -d " " -f 3 |${awk} -F . '{ print $1 "." $2 $3 }' | ${awk} '{ if ($0 > 3.19) print 3; else if ($0 < 2.015) print 1; else print 2 }'`
-         [ "$PV" = "" ] &&  PV=2
-         [ "${SYSTEM}" = "SunOS" ] && PV=0
-         expertmode_output "./chkproc -v -v -p $PV"
-         return 5
-      fi
+        if files=$(PATH="$path_for_tools" ./chkproc -p "${PV}" 2>&1); then
+            _not_found
+        else
+            _report "chkproc: Possible LKM Trojan installed (or chkproc failed)" "$files"
+        fi
+    fi
 
-      ### adore LKM
-      [ -r /proc/$KALLSYMS ] && \
-      if `${egrep} -i adore < /proc/$KALLSYMS >/dev/null 2>&1`; then
-         echo "Warning: Adore LKM installed"
-      fi
+    _start "for hidden directories using chkdirs"
+    if [ ! -x ./chkdirs ]; then
+        _warn "chkdirs not tested: can't exec chkdirs"
+    else
+        dirs=""
+        for i in tmp usr/share usr/bin usr/sbin lib usr/lib; do
+            if [ -d "${ROOTDIR}$i" ]; then
+                dirs="$dirs ${ROOTDIR}$i"
+            fi
+        done
+        if [ -z "$dirs" ]; then
+            _not_tested
+        elif files=$(./chkdirs $dirs 2>&1); then
+            _not_found
+        else
+            _report "chkdirs: Possible LKM Trojan installed (or chkdirs failed)" "$files"
+        fi
+    fi
 
-      ### sebek LKM (Adore based)
-      [ -r /proc/$KALLSYMS ] && \
-      if `${egrep} -i sebek < /proc/$KALLSYMS >/dev/null 2>&1`; then
-         echo "Warning: Sebek LKM installed"
-      fi
+    [ "$QUIET" != "t" ] && printn "Checking \`lkm'..."
+    [ "$QUIET" != "t" ] && echo "finished"
+}
 
-      ### knark LKM
-      if [ -d /proc/knark ]; then
-         echo "Warning: Knark LKM installed"
-      fi
-      if [ "$SYSTEM" = "Linux" ]; then 
-         F=`$ps -V 2>/dev/null | wc -w`
-         PV=`$ps -V 2>/dev/null| $cut -d " " -f $F |${awk} -F . '{ print $1 "." $2 $3 }' | ${awk} '{ if ($0 > 3.19) print 3; else if ($0 < 2.11) print 1; else print 2 }'`
-      fi 
-      [ "$PV" = "" ] &&  PV=2
-      [ "${SYSTEM}" = "SunOS" ] && PV=0
-      if [ "${DEBUG}" = "t" ]; then
-           ${echo} "*** PV=$PV ***"
-      fi
-      if ./chkproc -p ${PV}; then
-         if [ "${QUIET}" != "t" ]; then echo "chkproc: nothing detected"; fi
-      else
-         echo "chkproc: Warning: Possible LKM Trojan installed"
-      fi
-      dirs="/tmp"
-      for i in /usr/share /usr/bin /usr/sbin /lib; do
-         if [ "$(ls -ld $i | tr -s " " | cut -d " " -f 2)" -gt "1" ]; then 
-            [ -d $i ] && dirs="$dirs $i"
-         fi 
-      done
-      if ./chkdirs $dirs;  then
-         if [ "${QUIET}" != "t" ]; then echo "chkdirs: nothing detected"; fi
-      else
-	    echo "chkdirs: Warning: Possible LKM Trojan installed"
-      fi
-   else
-         if [ "${QUIET}" != "t" ]; then echo "chkproc: not tested"; fi
-   fi
 }
 
 aliens () {
