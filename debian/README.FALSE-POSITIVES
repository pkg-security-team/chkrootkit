* This file documents common false positives found by chkrootkit
  chkrootkit can never be perfect: it may miss things, and it may
  report things which are not actually an issue. The former means you
  should not rely only on chkrootkit for your security. The latter are
  called 'false positives'.

  You also have many ways to filter the output of chkrootkit to avoid
  being repeatedly notified of such false positives. See
  /etc/chkrootkit/chkrootkit.conf and the man page of chkrootkit(1) for
  details.

  This document lists common false positives flagged by chkrootkit,
  but, like chkrootkit itself, it can be neither complete nor fully
  accurate: being listed in this document does not prove there is no
  problem. Security involves judgement and understanding of what is
  meant to be: this cannot be fully automated.

* Common false positives produced by packages in Debian
  False positives can come from
  - legitimate but 'suspicious'-looking files under /usr/lib
  - legitimate network tools being flagged as packet sniffers
  - legitimate processes listening on well known ports
  - processes whose tty is not listed in utmp
  - processes reported as hidden
  - executables in /tmp
  - files in legitimate packages that are also found in rootkits

** 'suspicious' files in /usr/lib
   chkrootkit flags any file or directory under /usr/lib that begins
   with a '.'  as suspicious, because some rootkits use such files to
   evade detection.

   The following Debian packages are reported to include such files
   : blender
   : fail2ban        -  many files in /usr/lib/python3/dist-packages/fail2ban/tests/files/config/apache-auth
   : geomview
   : gnustep-make
   : libc6-dbg       -  /usr/lib/debug/.build-id
   : libreoffice-base - /usr/lib/libreoffice/share/.registry
   :  (many other libreoffice packages also add files under that directory)
   : openjdk-11-jre-headless:/usr/lib/jvm/.java-1.11.0-openjdk-amd64.jinfo
   : perl packages   - sometimes contain .packlist files
   : r-cran-hmisc
   : ruby-rubygems   - /usr/lib/ruby/vendor_ruby/rubygems/ssl_certs/.document
   : ruby-concurrent - /usr/lib/ruby/vendor_ruby/concurrent/.gitignore
   : scilab
   : smlnj

** packet sniffers
   chkrootkit reports anything that is listening to network traffic as
   a 'PACKET SNIFFER'. This includes common network managers including:
   - systemd-networkd
   - network-manager
   - wpa_supplicant
   - dhclient
   - dhcpd and dhcpcd (sometimes installed as dhcpd3, dhcpd4, dhcpcd5, etc)
   These are so common that the default setting of FILTER in
   /etc/chkrootkit/chkrootkit.conf ensures the message is the same
   whichever of these are running, and whichever order. You can also
   use the '-s' option to chkrootkit(1) to achieve a similar affect.

   Other potentially legitimate, but rarer, packet sniffers you might
   choose to run include: ethereal, knockd, p0f, pppoe, and tcpdump.

** use of well-known ports
   Some rootkits are known to listen on common ports that are also
   used by legitimate services. chkrootkit flags any such processes as
   suspicious, without further investigation. Because the bindshell
   rootkit listens on many ports, there can be many false positives
   from this test - there are too many to list in this file, but
   mosh(1) may trigger this. You can check what is running using ss(1)
   or netstat(1).

** processes without a tty in utmp
   The /var/run/utmp file contains details of running processes which
   is used by the w and who commands to show who is logged in. Because
   rootkits might wipe their data from utmp chkrootkit compares the
   output of ps(1) to utmp.

   Terminal multiplexers (tmux and screen) start windows/panes without
   recording their tty in utmp in a way that chkrootkit can detect,
   and processes started within them are flagged as 'suspicious' if
   the user is 'attached' when chkrootkit runs.

** hidden processes
   chkrootkit compares the list of running processes according to ps
   to the contents of /proc, flagging any mismatch as 'hidden'. Under
   very old kernels (2.6 and earlier) many false positives were
   produced from this check, but this is no longer the case.

   A process that happens to be ending just as chkrootkit is checking
   may produce such a false positive (Debian has patched chkproc to
   make this less likely, but some risk remains).

** executables in /tmp
   chkrootkit flags any executable file in /tmp as
   suspicious. Legitimate programmes or users may sometimes put
   temporary executables in /tmp, and these will be flagged as
   potential indicators of the Linux.DDoS malware.

   One example is that running 'apt source' (or 'apt-get source') with
   /tmp as the current directory will download and unpack Debian
   packages which will likely contain executables that will be flagged
   as suspicious.

** packages that share files with rootkits
   Many chkrootkit tests look for files known to be produced by
   rootkits. Because rootkits impersonate legitimate packages the
   files of some legitimate packages can be flagged as suspicious.

   We are aware of the following Debian packages in this situation:
   - ansible (/usr/lib/python3/dist-packages/ansible_collections/cyberark/conjur/dev/start.sh)
