Source: chkrootkit
Section: misc
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Marcos Fouces <marcos@debian.org>,
Standards-Version: 4.7.0
Build-Depends: debhelper-compat (= 13),
Homepage: https://www.chkrootkit.org/
Vcs-Git: https://salsa.debian.org/pkg-security-team/chkrootkit.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/chkrootkit
Rules-Requires-Root: no

Package: chkrootkit
Architecture: any
Built-Using: ${Built-Using},
Depends: ${misc:Depends}, ${shlibs:Depends},
Recommends:
 binutils,
 default-mta | mail-transport-agent,
 iproute2 | net-tools,
 mailx,
 procps,
 systemd-sysv | cron | anacron | cron-daemon,
Enhances:
 bcron,
 biff,
 bind9,
 bsdextrautils,
 busybox-syslogd,
 citadel-server,
 coreutils,
 courier-mta,
 cron,
 cronie,
 dma,
 esmtp-run,
 exim4-daemon-heavy,
 exim4-daemon-light,
 fetchmail,
 findutils,
 fingerd,
 finit-sysv,
 gpm,
 grep,
 hdparm,
 inetutils-syslogd,
 inetutils-telnetd,
 login,
 lsof,
 mailutils,
 masqmail,
 mingetty,
 msmtp-mta,
 net-tools,
 nullmailer,
 openbsd-inetd,
 opensmtpd,
 openssh-client,
 openssh-server,
 passwd,
 postfix,
 procps,
 psmisc,
 rpcbind,
 rsh-redone-server,
 runit-init,
 rust-findutils,
 ssmtp,
 systemd-cron,
 systemd-sysv,
 sysvinit-core,
 sysvinit-utils,
 tar,
 tcm,
 tcpd,
 tcpdump,
 traceroute,
 util-linux,
 wtmpdb,
Description: rootkit detector
 The chkrootkit security scanner searches for signs that the system is
 infected with a 'rootkit'. Rootkits are a form of malware that seek
 to exploit security flaws to grant unauthorised access to a
 computer or its services, generally for malicious purposes.
 .
 chkrootkit can identify signs of over 70 different rootkits (see the
 project's website for a list).
 .
 Please note that an automated tool like chkrootkit can never
 guarantee a system is uncompromised. Nor does every report always
 signify a genuine problem: human judgement and further investigation
 will always be needed to assure the security of your system.
